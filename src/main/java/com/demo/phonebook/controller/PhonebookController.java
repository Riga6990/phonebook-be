package com.demo.phonebook.controller;

import com.demo.phonebook.model.PhonebookModel;
import com.demo.phonebook.repo.PhonebookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//import javax.*;

@RestController
@RequestMapping(value = "/todo")
public class PhonebookController {
    @Autowired
    private PhonebookRepo phonebookRepo;

    @GetMapping
    public List<PhonebookModel> findAll() {
        return phonebookRepo.findAll();
    }

    @PostMapping
    public PhonebookModel save(@NonNull @RequestBody PhonebookModel phonebookModel) {
        return phonebookRepo.save(phonebookModel);
    }

    @PutMapping
    public PhonebookModel update(@NonNull @RequestBody PhonebookModel phonebookModel) {
        return phonebookRepo.save(phonebookModel);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        phonebookRepo.deleteById(id);
    }
}
