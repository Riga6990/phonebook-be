package com.demo.phonebook.repo;
import com.demo.phonebook.model.PhonebookModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhonebookRepo extends JpaRepository<PhonebookModel, Long>{

}
